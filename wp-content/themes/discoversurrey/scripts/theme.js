jQuery( document ).ready( function () {

	var startY = 200;

	var width = jQuery( 'html' ).outerWidth();

	jQuery( window ).scroll( function () {
		checkY();
	});

	function checkY() {
		if ( width > 544 ) {
			if ( jQuery( window ).scrollTop() > startY ) {
				jQuery( '.navbar' ).fadeIn( 2000 ).css( 'background', 'rgba(246,144,78,1)' );
			} else {
				jQuery( '.navbar' ).fadeIn( 2000 ).css( 'background', 'rgba(0,0,0,0)' );
			}
		}
	}

	checkY();

});



jQuery('#myCarousel').carousel({
  interval: 10000
})

jQuery('.carousel .carousel-item').each(function(){
  var next = jQuery(this).next();
  if (!next.length) {
    next = jQuery(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo(jQuery(this));

  if (next.next().length>0) {
    next.next().children(':first-child').clone().appendTo(jQuery(this));
  }
  else {
  	jQuery(this).siblings(':first').children(':first-child').clone().appendTo(jQuery(this));
  }
});



// // Dropdown Menu Fade
// jQuery(document).ready(function(){
//    jQuery(".dropdown").hover(
//        function() { jQuery('.dropdown-menu', this).fadeIn("fast");
//        },
//        function() { jQuery('.dropdown-menu', this).fadeOut("fast");
//    });
// });
