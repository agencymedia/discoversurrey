<?php get_header(); ?>

<header id="main">
	<div class="hidden-sm-down">
	<?php if ( get_field( 'banner_media' ) == 'Video' ) { ?>
		<video autoplay muted loop poster="<?php the_field( 'banner_image' ); ?>" id="banner-video">
			<source src="<?php the_field( 'banner_video' ); ?>" type="video/mp4">
		</video>
	<?php } else { ?>
		<img src="<?php the_field( 'banner_image' ); ?>" class="img-fluid">
	<?php } ?>
	</div>
	<div class="hidden-md-up">
		<img src="<?php the_field( 'banner_image' ); ?>" class="img-fluid">
	</div>
</header>

<section id="section-intro">
	<div class="container">
		<!--<div class="row">
			<div class="col-sm-12">
				<?php the_field( 'intro_text' ); ?>
			</div>
		</div>-->
		<div class="row">
			<?php $i = 0; $col_size = array(5, 5, 4, 6, 6, 4); if( have_rows( 'intro_images' ) ): while ( have_rows( 'intro_images' ) ) : the_row(); ?>
			<div class="col-xs-12 col-sm-6 col-md-push-1 col-md-<?php echo $col_size[$i]; ?> intro-image">
				<a href="<?php the_sub_field( 'intro_link' ); ?>"><img src="<?php the_sub_field( 'intro_image' ); ?>" class="img-fluid"></a>
			</div>
			<?php $i++; endwhile; endif; ?>
		</div>
	</div>
</section>

<section id="section-gateway">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<img src="<?php bloginfo( 'template_directory' ); ?>/images/discover-surrey-header-2.png" alt="Discover Surrey" class="img-fluid gateway-image">
				<div class="col-sm-12 col-md-push-2 col-md-8"><?php the_field( 'gateway_text' ); ?></div>
			</div>
		</div>
		<div class="row gateway-links">
			<?php if( have_rows( 'gateway_links' ) ): while ( have_rows( 'gateway_links' ) ) : the_row(); ?>
			<div class="col-xs-12 col-sm-6 col-md-3">
				<a href="<?php the_sub_field( 'gateway_link' ); ?>" class="gateway-link"><span><?php the_sub_field( 'gateway_text' ); ?></span></a>
			</div>
			<?php endwhile; endif; ?>
		</div>
	</div>
</section>

<section id="section-neighbourhoods">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="col-sm-12 col-md-push-2 col-md-8"><?php the_field( 'neighbourhoods_text' ); ?></div>
			</div>
		</div>
	</div>
</section>

<section id="section-map">
		<div class="row">
			<div class="col-sm-12">
				<?php echo do_shortcode("[wpgmza id='1']"); ?>
			</div>
		</div>
</section>

<section id="section-events">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<img src="<?php bloginfo( 'template_directory' ); ?>/images/header-events.png" alt="Discover Surrey" class="img-fluid events-image">
				<div class="col-sm-12 col-md-push-2 col-md-8"><?php the_field( 'events_text' ); ?></div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<p>events calendar</p>
			</div>
		</div>
	</div>
</section>

<section id="section-stories">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 col-md-push-3 col-md-6">
				<?php the_field( 'stories_text' ); ?>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				  <div class="carousel slide" id="myCarousel" data-ride="carousel">
				    <div class="carousel-inner">
				      <div id="stories-slide" class="carousel-item active">
				      <?php
							$args = array('post_type' => 'stories');
							$query = new WP_Query($args);
							while($query -> have_posts()) : $query -> the_post();
						?>
				        <div class="col-md-4" style="background-image:url('<?php the_field('stories_home_page_image'); ?>');">
									<h2><?php the_title(); ?></h2>
									<h4><?php the_field('stories_home_page_subhead'); ?></h4>
									<!--<img src="<?php the_field('accomm_image'); ?>" class="card img-fluid">-->
						</div>
						<?php endwhile; ?>
				      </div>


				      <div class="carousel-item">
				        <div class="col-md-4"><a href="#"><img src="http://placehold.it/350x150" class="img-fluid"></a></div>
				      </div>
				      <div class="carousel-item">
				        <div class="col-md-4"><a href="#"><img src="http://placehold.it/350x150" class="img-fluid"></a></div>
				      </div>
				      <div class="carousel-item">
				        <div class="col-md-4"><a href="#"><img src="http://placehold.it/350x150" class="img-fluid"></a></div>
				      </div>
				      <div class="carousel-item">
				        <div class="col-md-4"><a href="#"><img src="http://placehold.it/350x150" class="img-fluid"></a></div>
				      </div>
				      <div class="carousel-item">
				        <div class="col-md-4"><a href="#"><img src="http://placehold.it/350x150" class="img-fluid"></a></div>
				      </div>
				    </div>
				    <!--<a class="left carousel-control text-xs-left" href="#myCarousel" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
				    <a class="right carousel-control text-xs-right" href="#myCarousel" data-slide="next"><i class="fa fa-chevron-right"></i></a>-->
				</div>
				<!--<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner" role="listbox">
						<div class="carousel-item active">
							<div class="col-md-3">
								<img src="https://unsplash.it/200" alt="First slide" title="First">
								<div class="carousel-caption">
									<h3>One</h3>
									<p>...</p>
								</div>
							</div>
						</div>
						<div class="carousel-item">
							<div class="col-md-3">
								<img src="https://unsplash.it/200" alt="Second slide" title="Second">
								<div class="carousel-caption">
									<h3>Two</h3>
									<p>...</p>
								</div>
							</div>
						</div>
						<div class="carousel-item">
							<div class="col-md-3">
								<img src="https://unsplash.it/200" alt="Third slide" title="Third">
								<div class="carousel-caption">
									<h3>Three</h3>
									<p>...</p>
								</div>
							</div>
						</div>
						<div class="carousel-item">
							<div class="col-md-3">
								<img src="https://unsplash.it/200" alt="Fourth slide" title="Fourth">
								<div class="carousel-caption">
									<h3>Four</h3>
									<p>...</p>
								</div>
							</div>
						</div>
						<div class="carousel-item">
							<div class="col-md-3">
								<img src="https://unsplash.it/200" alt="Fifth slide" title="Fifth">
								<div class="carousel-caption">
									<h3>Five</h3>
									<p>...</p>
								</div>
							</div>
						</div>
						<div class="carousel-item">
							<div class="col-md-3">
								<img src="https://unsplash.it/200" alt="Sixth slide" title="Sixth">
								<div class="carousel-caption">
									<h3>Six</h3>
									<p>...</p>
								</div>
							</div>
						</div>
						<div class="carousel-item">
							<div class="col-md-3">
								<img src="https://unsplash.it/200" alt="Seventh slide" title="Seventh">
								<div class="carousel-caption">
									<h3>Seven</h3>
									<p>...</p>
								</div>
							</div>
						</div>
					</div>
				</div>-->


			</div>
		</div>
	</div>
</section>

<section id="section-social">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<?php the_field( 'social_text' ); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<img src="<?php bloginfo( 'template_directory' ); ?>/images/discover-surrey-footer.png" alt="Discover Surrey" class="img-fluid social-image">
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
/*jQuery( '.carousel .carousel-item' ).each( function() {
	var next = jQuery( this ).next();
	if ( !next.length ) {
		next = jQuery( this ).siblings( ':first' );
	}
	next.children( ':first-child' ).clone().appendTo( jQuery( this ) );
	if (next.next().length>0 ) {
		next.next().children( ':first-child' ).clone().appendTo( jQuery( this ) );
	} else {
		jQuery( this ).siblings( ':first' ).children( ':first-child' ).clone().appendTo( jQuery( this ) );
	}
});*/
</script>
<?php get_footer(); ?>
