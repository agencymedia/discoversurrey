<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<link rel="shortcut icon" href="<?php bloginfo( 'template_directory' );?>/images/favicon.ico" />
	<title>
		<?php bloginfo( 'name' );?>
		<?php wp_title( '|', true, 'left' ); ?>
	</title>
	<link rel="shortcut icon" href="<?php bloginfo( 'template_directory' );?>/images/favicon.ico" />
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<nav class="navbar navbar-fixed-top navbar-dark">
	<div class="container">
		<div class="navbar-header">
		<a class="navbar-brand" href="/"><img src="<?php bloginfo( 'template_directory' ); ?>/images/discover-surrey-logo.png" alt="Discover Surrey" class="img-fluid"></a>

		<button class="navbar-toggler hidden-md-up pull-xs-right" type="button" data-toggle="collapse" data-target="#nav-collapse" aria-controls="nav-collapse" aria-expanded="false" aria-label="Toggle navigation">
	&#9776;</button>
		</div>

		<div class="hidden-sm-down">
	    	<ul class="nav nav-inline text-xs-center text-md-right">
				<li class="nav-item dropdown mega-dropdown">
				<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">Explore</a>
				    <ul class="dropdown-menu mega-dropdown-menu row" role="menu">
					    <div class="card-group">
							<div class="card sub-nav-social">
								<ul>
									<li><a href="#" class="sub-nav-social-facebook"><img src="<?php bloginfo( 'template_directory' ); ?>/images/fb-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-twitter"><img src="<?php bloginfo( 'template_directory' ); ?>/images/tw-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-youtube"><img src="<?php bloginfo( 'template_directory' ); ?>/images/yt-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-instagram"><img src="<?php bloginfo( 'template_directory' ); ?>/images/in-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-pinterest"><img src="<?php bloginfo( 'template_directory' ); ?>/images/pn-icon.png" width="50" alt=""></a></li>
								</ul>
							</div>
							<div class="card sub-nav-links">
									<ul>
										<li class="top-level"><a href="/explore">Explore</a></li>
										<li><a href="#">The Great Outdoors</a></li>
											<ul>
												<li><a href="/stories/true-athlete">True Athlete</a></li>
												<li><a href="#">For the Birds</a></li>
												<li><a href="#">Golf Surrey</a></li>
											</ul>
										<li><a href="#">Parks, Trails & Gardens</a></li>
											<ul>
												<li><a href="#">Green Timbers Urban Forest</a></li>
												<li><a href="#">Redwood Park</a></li>
												<li><a href="#">Cresent Beach</a></li>
											</ul>

												<li><a href="#">The Best of Surrey</a></li>
													<ul>
														<li><a href="#">Our Favourite Secret Gardens</a></li>
														<li><a href="#">Our Favourite Date Night Venues</a></li>
														<li><a href="#">Our Favourite Breakfast Spots</a></li>
														<li><a href="#">Our Favourite Caffeine Kicks</a></li>
														<li><a href="#">Our Favourite U-picks</a></li>
														<li><a href="#">Our Favourite Art Galleries & Studios</a></li>
													</ul>
									</ul>
							</div>
							<div class="card sub-nav-links">
									<span>Subscribe</span>
									<br>Subscribe to our newsletter
									<li><a href="#">Our Neighborhoods</a></li>
										<ul>
											<li><a href="#">Cloverdale</a></li>
												<ul>
													<li><a href="#">Cloverdale Rodeo</a></li>
													<li><a href="#">TrueWinner</a></li>
													<li><a href="#">Honeybee Centre</a></li>
												</ul>
											<li><a href="#">Fleetwood</a></li>
												<ul>
													<li><a href="#">Edith & Arthur</a></li>
													<li><a href="#">TrueBarista</a></li>
													<li><a href="#">A Local's Perspective</a></li>
												</ul>
											<li><a href="#">Guildford</a></li>
												<ul>
													<li><a href="#">Guildford Shopping Centre</a></li>
													<li><a href="#">Guildford Rec Centre</a></li>
													<li><a href="#">Charlie's Tree</a></li>
												</ul>
											<li><a href="#">Newton</a></li>
												<ul>
													<li><a href="#">Payal Pizza</a></li>
													<li><a href="#">Love at 1st ZBite</a></li>
													<li><a href="#">TrueChef</a></li>
												</ul>
											<li><a href="#">North Surrey</a></li>
												<ul>
													<li><a href="#">Central City Brewery</a></li>
													<li><a href="#">Surrey Urban Farmers Market</a></li>
													<li><a href="#">Green Timbers Urban Forest</a></li>
												</ul>
											<li><a href="#">South Surrey</a></li>
												<ul>
													<li><a href="#">Historic Stewart Farm</a></li>
													<li><a href="#">Crescent Beach</a></li>
													<li><a href="#">Redwood Park</a></li>
												</ul>
										</ul>
							</div>
							<div class="card sub-nav-links">
									<span>Download</span>
									<br>Check out our 2016 Discovery Guide
									<br><a href="#"><img src="<?php bloginfo( 'template_directory' ); ?>/images/Visitor-Guide.png"></a>
							</div>
						</div>
				    </ul>
				</li>
				<li class="nav-item dropdown mega-dropdown">
				<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">Do &amp; See</a>
				    <ul class="dropdown-menu mega-dropdown-menu row" role="menu">
					    <div class="card-group">
							<div class="card sub-nav-social">
								<ul>
									<li><a href="#" class="sub-nav-social-facebook"><img src="<?php bloginfo( 'template_directory' ); ?>/images/fb-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-twitter"><img src="<?php bloginfo( 'template_directory' ); ?>/images/tw-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-youtube"><img src="<?php bloginfo( 'template_directory' ); ?>/images/yt-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-instagram"><img src="<?php bloginfo( 'template_directory' ); ?>/images/in-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-pinterest"><img src="<?php bloginfo( 'template_directory' ); ?>/images/pn-icon.png" width="50" alt=""></a></li>
								</ul>
							</div>
							<div class="card sub-nav-links">
									<ul>
										<li class="top-level"><a href="/do-and-see">Do &amp; See</a></li>
										<li><a href="#">Arts & Culture</a></li>
										<ul>
											<li><a href="#">Little Cabin, Big Dreams</a></li>
											<li><a href="#">Beach House Theatre</a></li>
											<li><a href="#">Historic Stewart Farm</a></li>
										</ul>
										<li><a href="#">Indoor Activities</a></li>
											<ul>
												<li><a href="#">Elements Casino</a></li>
												<li><a href="#">Guildford Rec Centre</a></li>
												<li><a href="#">Rainy Days for Families</a></li>
											</ul>
										<li><a href="#">Shopping</a></li>
											<ul>
												<li><a href="#">Payal Plaza</a><li>
												<li><a href="#">Potters Nursery</a></li>
												<li><a href="#">Guildford Shopping Centre</a></li>
											</ul>
										<li><a href="#">Family Fun</a></li>
											<ul>
												<li><a href="#">Kid Approved</a></li>
												<li><a href="#">Honeybee Centre</a></li>
												<li><a href="#">Orange is the New Awesome</a></li>
											</ul>
										<li><a href="#">Festivals & Events</a></li>
											<ul>
												<li><a href="#">Fusion Fest</a></li>
												<li><a href="#">Cloverdale Rodeo</a></li>
												<li><a href="#">Party for the Planet</a></li>
											</ul>
									</ul>
							</div>
							<div class="card sub-nav-links">
									<span>Subscribe</span>
									<br>Subscribe to our newsletter
							</div>
							<div class="card sub-nav-links">
									<span>Download</span>
									<br>Check out our 2016 Discovery Guide
									<br><a href="#"><img src="<?php bloginfo( 'template_directory' ); ?>/images/Visitor-Guide.png"></a>
							</div>
						</div>
				    </ul>
				</li>
				<li class="nav-item dropdown mega-dropdown">
				<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">Taste</a>
				    <ul class="dropdown-menu mega-dropdown-menu row" role="menu">
					    <div class="card-group">
							<div class="card sub-nav-social">
								<ul>
									<li><a href="#" class="sub-nav-social-facebook"><img src="<?php bloginfo( 'template_directory' ); ?>/images/fb-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-twitter"><img src="<?php bloginfo( 'template_directory' ); ?>/images/tw-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-youtube"><img src="<?php bloginfo( 'template_directory' ); ?>/images/yt-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-instagram"><img src="<?php bloginfo( 'template_directory' ); ?>/images/in-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-pinterest"><img src="<?php bloginfo( 'template_directory' ); ?>/images/pn-icon.png" width="50" alt=""></a></li>
								</ul>
							</div>
							<div class="card sub-nav-links">
									<ul>
										<li class="top-level"><a href="/taste">Taste</a></li>
										<li><a href="#">Wineries & Breweries</a></li>
											<ul>
												<li><a href="#">Central City Brewery</a></li>
												<li><a href="#">TrueSommelier</a></li>
												<li><a href="#">White Rock Brewing</a></li>
											</ul>
										<li><a href="#">Restaurants & Eateries</a></li>
											<ul>
												<li><a href="#">TrueChef</a></li>
												<li><a href="#">West Coast = Best Coast</a></li>
												<li><a href="#">TrueWinner</a></li>
											</ul>
										<li><a href="#">Grill Pubs & Bars</a></li>
											<ul>
												<li><a href="#">Hawthorne Beer Market</a></li>
												<li><a href="#">Edith & Arthur</a></li>
												<li><a href="#">Our Favourite Happy Hours</a></li>
											</ul>
										<li><a href="#">Ethnic Cuisine</a></li>
											<ul>
												<li><a href="#">Global Gourmet</a></li>
												<li><a href="#">Love at 1st Bite</a></li>
												<li><a href="#">Hidden Treasures</a></li>
											</ul>
										<li><a href="#">Coffee & Sweets</a></li>
											<ul>
												<li><a href="#">Mink Chocolate</a></li>
												<li><a href="#">Sugar Patisserie</a></li>
												<li><a href="#">TrueBarista</a></li>
											</ul>
										<li><a href="#">Farms & Markets</a></li>
											<ul>
												<li><a href="#">Food Community</a></li>
												<li><a href="#">And on his farm he had some cows...</a></li>
												<li><a href="#">Surrey Urban Farmers Market</a></li>
											</ul>
										<li><a href="#">Specialty</a></li>
											<ul>
												<li><a href="#">All of Oils</a></li>
												<li><a href="#">Beast & Brine</a></li>
												<li><a href="">Antony & Sons</a></li>
											</ul>
									</ul>
							</div>
							<div class="card sub-nav-links">
									<span>Subscribe</span>
									<br>Subscribe to our newsletter
							</div>
							<div class="card sub-nav-links">
									<span>Download</span>
									<br>Check out our 2016 Discovery Guide
									<br><a href="#"><img src="<?php bloginfo( 'template_directory' ); ?>/images/Visitor-Guide.png"></a>
							</div>
						</div>
				    </ul>
				</li>
				<li class="nav-item dropdown mega-dropdown">
				<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">Stay</a>
				    <ul class="dropdown-menu mega-dropdown-menu row" role="menu">
					    <div class="card-group">
							<div class="card sub-nav-social">
								<ul>
									<li><a href="#" class="sub-nav-social-facebook"><img src="<?php bloginfo( 'template_directory' ); ?>/images/fb-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-twitter"><img src="<?php bloginfo( 'template_directory' ); ?>/images/tw-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-youtube"><img src="<?php bloginfo( 'template_directory' ); ?>/images/yt-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-instagram"><img src="<?php bloginfo( 'template_directory' ); ?>/images/in-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-pinterest"><img src="<?php bloginfo( 'template_directory' ); ?>/images/pn-icon.png" width="50" alt=""></a></li>
								</ul>
							</div>
							<div class="card sub-nav-links">
									<ul>
										<li class="top-level"><a href="/accommodation">Stay</a></li>
										<li><a href="/hotel">Hotels</a></li>
										<li><a href="/motel">Motels</a></li>
										<li><a href="#">Bed & Breakfasts</a></li>
										<li><a href="#">Camping & RV</a></li>
										<li><a href="#">Packages & Deals</a></li>
									</ul>
							</div>
							<div class="card sub-nav-links">
									<span>Subscribe</span>
									<br>Subscribe to our newsletter
							</div>
							<div class="card sub-nav-links">
									<span>Download</span>
									<br>Check out our 2016 Discovery Guide
									<br><a href="#"><img src="<?php bloginfo( 'template_directory' ); ?>/images/Visitor-Guide.png"></a>
							</div>
						</div>
				    </ul>
				</li>
				<li class="nav-item dropdown mega-dropdown">
				<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">Plan Your Trip</a>
				    <ul class="dropdown-menu mega-dropdown-menu row" role="menu">
					    <div class="card-group">
							<div class="card sub-nav-social">
								<ul>
									<li><a href="#" class="sub-nav-social-facebook"><img src="<?php bloginfo( 'template_directory' ); ?>/images/fb-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-twitter"><img src="<?php bloginfo( 'template_directory' ); ?>/images/tw-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-youtube"><img src="<?php bloginfo( 'template_directory' ); ?>/images/yt-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-instagram"><img src="<?php bloginfo( 'template_directory' ); ?>/images/in-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-pinterest"><img src="<?php bloginfo( 'template_directory' ); ?>/images/pn-icon.png" width="50" alt=""></a></li>
								</ul>
							</div>
							<div class="card sub-nav-links">
									<ul>
										<li class="top-level"><a href="/plan-your-trip">Plan Your Trip</a></li>
										<li><a href="#">Surrey's Story</a></li>
										<li><a href="#">Getting Here</a></li>
										<li><a href="#">Visitor Services</a></li>
										<li><a href="#">Map</a></li>
										<li><a href="#">Contact Us</a></li>
									</ul>
							</div>
							<div class="card sub-nav-links">
									<span>Subscribe</span>
									<br>Subscribe to our newsletter
							</div>
							<div class="card sub-nav-links">
									<span>Download</span>
									<br>Check out our 2016 Discovery Guide
									<br><a href="#"><img src="<?php bloginfo( 'template_directory' ); ?>/images/Visitor-Guide.png"></a>
							</div>
						</div>
				    </ul>
				</li>
				<!-- <li class="nav-item">
					<a href="#" class="nav-search"><img src="<?php bloginfo( 'template_directory' ); ?>/images/icon-search.png" alt=""></a>
				</li>
				<li class="nav-item">
					<a href="#" class="nav-faves"><img src="<?php bloginfo( 'template_directory' ); ?>/images/icon-faves.png" alt=""></a>
				</li> -->
			</ul>
		</div>
	</div>
</nav>



<div class="collapse navbar-toggleable-sm pull-xs-right hidden" id="nav-collapse">
	<ul class="nav nav-inline text-xs-center text-md-right">
		<li class="nav-item">
			<a class="nav-link" href="/explore/">Explore</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/do-see/">Do &amp; See</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/taste/">Taste</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/stay/">Stay</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/plan-your-trip/">Plan Your Trip</a>
		</li>
	</ul>
</div>
