<?php get_header(); ?>

<?php $header_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full' ); ?>
<section style="background-image: url(<?php echo $header_image[0]; ?>);" id="section-header">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</section>

<section id="section-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
				<div class="col-md-6">
					<p><?php the_content(); ?></p>
				</div>
				<div class="col-md-5 col-md-push-1">
					<div class="col-md-12">
						<img src="<?php the_field('stories_sideimage1'); ?>" class="img-fluid">
					</div>
					<div class="col-md-12">
						<img src="<?php the_field('stories_sideimage2'); ?>" class="img-fluid">
					</div>
				</div>

				<?php endwhile; endif; ?>
				
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?> 