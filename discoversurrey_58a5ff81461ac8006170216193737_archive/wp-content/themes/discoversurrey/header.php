<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<link rel="shortcut icon" href="<?php bloginfo( 'template_directory' );?>/images/favicon.ico" />

	<title>
		<?php bloginfo( 'name' );?>
		<?php wp_title( '|', true, 'left' ); ?>
	</title>
	<link rel="shortcut icon" href="<?php bloginfo( 'template_directory' );?>/images/favicon.ico" />

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<nav class="navbar navbar-fixed-top navbar-dark">
	<div class="container">
		<div class="navbar-header">
		<a class="navbar-brand" href="/"><img src="<?php bloginfo( 'template_directory' ); ?>/images/discover-surrey-logo.png" alt="Discover Surrey" class="img-fluid"></a>
		
		<button class="navbar-toggler hidden-md-up pull-xs-right" type="button" data-toggle="collapse" data-target="#nav-collapse" aria-controls="nav-collapse" aria-expanded="false" aria-label="Toggle navigation">
	&#9776;</button>
		</div>

		<div class="hidden-sm-down">
	    	<ul class="nav nav-inline text-xs-center text-md-right">
				<li class="nav-item dropdown mega-dropdown">
				<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">Explore</a>
				    <ul class="dropdown-menu mega-dropdown-menu row" role="menu">
					    <div class="card-group">
							<div class="card sub-nav-social">
								<ul>
									<li><a href="#" class="sub-nav-social-facebook"><img src="<?php bloginfo( 'template_directory' ); ?>/images/fb-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-twitter"><img src="<?php bloginfo( 'template_directory' ); ?>/images/tw-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-youtube"><img src="<?php bloginfo( 'template_directory' ); ?>/images/yt-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-instagram"><img src="<?php bloginfo( 'template_directory' ); ?>/images/in-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-pinterest"><img src="<?php bloginfo( 'template_directory' ); ?>/images/pn-icon.png" width="50" alt=""></a></li>
								</ul>
							</div>
							<div class="card sub-nav-links">
									<ul>
										<li class="top-level"><a href="/explore">Explore</a></li>
										<li><a href="#">The Great Outdoors</a></li>
											<ul>
												<li><a href="/stories/true-athlete">True Athlete</a></li>
												<li><a href="#">For the Birds</a></li>
												<li><a href="#">Golf Surrey</a></li>
											</ul>
										<li><a href="#">Parks, Trails & Gardens</a></li>
											<ul>
												<li><a href="#">Green Timbers Urban Forest</a></li>
												<li><a href="#">Redwood Park</a></li>
												<li><a href="#">Cresent Beach</a></li>
											</ul>
									</ul>
							</div>
							<div class="card sub-nav-links">
									<span>Subscribe</span>
									<br>Subscribe to our newsletter
							</div>
							<div class="card sub-nav-links">
									<span>Download</span>
									<br>Check out our 2016 Discovery Guide
									<br><a href="#"><img src="<?php bloginfo( 'template_directory' ); ?>/images/Visitor-Guide.png"></a>
							</div>
						</div>
				    </ul>
				</li>
				<li class="nav-item dropdown mega-dropdown">
				<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">Do &amp; See</a>
				    <ul class="dropdown-menu mega-dropdown-menu row" role="menu">
					    <div class="card-group">
							<div class="card sub-nav-social">
								<ul>
									<li><a href="#" class="sub-nav-social-facebook"><img src="<?php bloginfo( 'template_directory' ); ?>/images/fb-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-twitter"><img src="<?php bloginfo( 'template_directory' ); ?>/images/tw-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-youtube"><img src="<?php bloginfo( 'template_directory' ); ?>/images/yt-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-instagram"><img src="<?php bloginfo( 'template_directory' ); ?>/images/in-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-pinterest"><img src="<?php bloginfo( 'template_directory' ); ?>/images/pn-icon.png" width="50" alt=""></a></li>
								</ul>
							</div>
							<div class="card sub-nav-links">
									<ul>
										<li class="top-level"><a href="/do-and-see">Do &amp; See</a></li>
										<li><a href="#">Arts & Culture</a></li>
										<li><a href="#">Indoor Activities</a></li>
										<li><a href="#">Shopping</a></li>
										<li><a href="#">Family Fun</a></li>
										<li><a href="#">Festivals & Events</a></li>
									</ul>
							</div>
							<div class="card sub-nav-links">
									<span>Subscribe</span>
									<br>Subscribe to our newsletter
							</div>
							<div class="card sub-nav-links">
									<span>Download</span>
									<br>Check out our 2016 Discovery Guide
									<br><a href="#"><img src="<?php bloginfo( 'template_directory' ); ?>/images/Visitor-Guide.png"></a>
							</div>
						</div>
				    </ul>
				</li>
				<li class="nav-item dropdown mega-dropdown">
				<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">Taste</a>
				    <ul class="dropdown-menu mega-dropdown-menu row" role="menu">
					    <div class="card-group">
							<div class="card sub-nav-social">
								<ul>
									<li><a href="#" class="sub-nav-social-facebook"><img src="<?php bloginfo( 'template_directory' ); ?>/images/fb-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-twitter"><img src="<?php bloginfo( 'template_directory' ); ?>/images/tw-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-youtube"><img src="<?php bloginfo( 'template_directory' ); ?>/images/yt-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-instagram"><img src="<?php bloginfo( 'template_directory' ); ?>/images/in-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-pinterest"><img src="<?php bloginfo( 'template_directory' ); ?>/images/pn-icon.png" width="50" alt=""></a></li>
								</ul>
							</div>
							<div class="card sub-nav-links">
									<ul>
										<li class="top-level"><a href="/taste">Taste</a></li>
										<li><a href="#">Wineries & Breweries</a></li>
										<li><a href="#">Restaurants & Eateries</a></li>
										<li><a href="#">Grill Pubs & Bars</a></li>
										<li><a href="#">Ethnic Cuisine</a></li>
										<li><a href="#">Coffee & Sweets</a></li>
										<li><a href="#">Farms & Markets</a></li>
										<li><a href="#">Specialty</a></li>
									</ul>
							</div>
							<div class="card sub-nav-links">
									<span>Subscribe</span>
									<br>Subscribe to our newsletter
							</div>
							<div class="card sub-nav-links">
									<span>Download</span>
									<br>Check out our 2016 Discovery Guide
									<br><a href="#"><img src="<?php bloginfo( 'template_directory' ); ?>/images/Visitor-Guide.png"></a>
							</div>
						</div>
				    </ul>
				</li>
				<li class="nav-item dropdown mega-dropdown">
				<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">Stay</a>
				    <ul class="dropdown-menu mega-dropdown-menu row" role="menu">
					    <div class="card-group">
							<div class="card sub-nav-social">
								<ul>
									<li><a href="#" class="sub-nav-social-facebook"><img src="<?php bloginfo( 'template_directory' ); ?>/images/fb-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-twitter"><img src="<?php bloginfo( 'template_directory' ); ?>/images/tw-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-youtube"><img src="<?php bloginfo( 'template_directory' ); ?>/images/yt-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-instagram"><img src="<?php bloginfo( 'template_directory' ); ?>/images/in-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-pinterest"><img src="<?php bloginfo( 'template_directory' ); ?>/images/pn-icon.png" width="50" alt=""></a></li>
								</ul>
							</div>
							<div class="card sub-nav-links">
									<ul>
										<li class="top-level"><a href="/accommodation">Stay</a></li>
										<li><a href="/hotel">Hotels</a></li>
										<li><a href="/motel">Motels</a></li>
										<li><a href="#">Bed & Breakfasts</a></li>
										<li><a href="#">Camping & RV</a></li>
										<li><a href="#">Packages & Deals</a></li>
									</ul>
							</div>
							<div class="card sub-nav-links">
									<span>Subscribe</span>
									<br>Subscribe to our newsletter
							</div>
							<div class="card sub-nav-links">
									<span>Download</span>
									<br>Check out our 2016 Discovery Guide
									<br><a href="#"><img src="<?php bloginfo( 'template_directory' ); ?>/images/Visitor-Guide.png"></a>
							</div>
						</div>
				    </ul>
				</li>
				<li class="nav-item dropdown mega-dropdown">
				<a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">Plan Your Trip</a>
				    <ul class="dropdown-menu mega-dropdown-menu row" role="menu">
					    <div class="card-group">
							<div class="card sub-nav-social">
								<ul>
									<li><a href="#" class="sub-nav-social-facebook"><img src="<?php bloginfo( 'template_directory' ); ?>/images/fb-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-twitter"><img src="<?php bloginfo( 'template_directory' ); ?>/images/tw-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-youtube"><img src="<?php bloginfo( 'template_directory' ); ?>/images/yt-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-instagram"><img src="<?php bloginfo( 'template_directory' ); ?>/images/in-icon.png" width="50" alt=""></a></li>
									<li><a href="#" class="sub-nav-social-pinterest"><img src="<?php bloginfo( 'template_directory' ); ?>/images/pn-icon.png" width="50" alt=""></a></li>
								</ul>
							</div>
							<div class="card sub-nav-links">
									<ul>
										<li class="top-level"><a href="/plan-your-trip">Plan Your Trip</a></li>
										<li><a href="#">Surrey's Story</a></li>
										<li><a href="#">Getting Here</a></li>
										<li><a href="#">Visitor Services</a></li>
										<li><a href="#">Map</a></li>
										<li><a href="#">Contact Us</a></li>
									</ul>
							</div>
							<div class="card sub-nav-links">
									<span>Subscribe</span>
									<br>Subscribe to our newsletter
							</div>
							<div class="card sub-nav-links">
									<span>Download</span>
									<br>Check out our 2016 Discovery Guide
									<br><a href="#"><img src="<?php bloginfo( 'template_directory' ); ?>/images/Visitor-Guide.png"></a>
							</div>
						</div>
				    </ul>
				</li>
				<li class="nav-item">
					<a href="#" class="nav-search"><img src="<?php bloginfo( 'template_directory' ); ?>/images/icon-search.png" alt=""></a>
				</li>
				<li class="nav-item">
					<a href="#" class="nav-faves"><img src="<?php bloginfo( 'template_directory' ); ?>/images/icon-faves.png" alt=""></a>
				</li>
			</ul>
		</div>
	</div>
</nav>



<div class="collapse navbar-toggleable-sm pull-xs-right hidden" id="nav-collapse">
	<ul class="nav nav-inline text-xs-center text-md-right">
		<li class="nav-item">
			<a class="nav-link" href="/explore/">Explore</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/do-see/">Do &amp; See</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/taste/">Taste</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/stay/">Stay</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" href="/plan-your-trip/">Plan Your Trip</a>
		</li>
	</ul>
</div>