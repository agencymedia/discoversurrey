<?php

/* Register stylesheets */

function theme_styles() {

	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/style.css' );

}
add_action( 'wp_enqueue_scripts', 'theme_styles' );



/* Register scripts */

function theme_js() {

	wp_deregister_script( 'jquery' );
	wp_register_script( 'jquery', includes_url( '/js/jquery/jquery.js' ), NULL, NULL, false );
    wp_enqueue_script( 'jquery' );

	wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() . '/scripts/bootstrap.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'jquery_ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'theme_scripts', get_template_directory_uri() . '/scripts/theme.js', array( 'jquery' ), '', true );

}
add_action( 'wp_enqueue_scripts', 'theme_js' );



/* Events columns */

add_action( "manage_calendar_posts_custom_column",  "calendar_custom_columns", 10, 2 );
add_filter( "manage_calendar_posts_columns", "calendar_edit_columns" );
 
function calendar_edit_columns( $columns ){
	$columns = array(
		"cb" => "<input type='checkbox' />",
		"title" => "Title",
		"featured" => "Featured",
		"event_start" => "Start Date and Time",
		"event_end" => "End Date and Time",
		"date" => "Last modified",
	);
	return $columns;
}

function calendar_custom_columns( $column ){
	global $post;
	//$custom = get_post_custom();
	
	switch ($column) {
		case "featured" :
			if ( get_field( 'event_featured' ) ) {
				echo 'Yes';
			} else {
				echo 'No';
			}
		break;
		case "event_start" :
			$event_start_date = get_field( 'event_start_date' );
			$event_start_time = get_field( 'event_start_time' );
			echo $event_start_date . '<br/><em>' . $event_start_time . '</em>';
		break;
		case "event_end" :
			$event_end_date = get_field( 'event_end_date' );
			$event_end_time = get_field( 'event_end_time' );
			echo $event_end_date . '<br/><em>' . $event_end_time . '</em>';
		break;
	}
}

function calendar_column_register_sortable( $columns ){
	$columns['event_start'] = 'event_start';
	return $columns;
}

add_filter( "manage_edit-calendar_sortable_columns", "calendar_column_register_sortable" );



/* Register widgets */

if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'name' => 'Footer - Subscribe',
		'description' => __( 'Display email sign up form in the footer' ),
		'before_widget' => '<div id="footer-subscribe" class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));
	register_sidebar(array(
		'name' => 'Footer - Magazine',
		'description' => __( 'Display Discover Magazine in the footer' ),
		'before_widget' => '<div id="footer-magazine" class="widget">',
		'after_widget' => '</div>',
	));
	register_sidebar(array(
		'name' => 'Footer - Social',
		'description' => __( 'Display social media links the footer' ),
		'before_widget' => '<div id="footer-social" class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));
	register_sidebar(array(
		'name' => 'Footer - Corporate',
		'description' => __( 'Display Corporate links in the footer' ),
		'before_widget' => '<div id="footer-corporate" class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));
	register_sidebar(array(
		'name' => 'Footer - Explore',
		'description' => __( 'Display Explore links in the footer' ),
		'before_widget' => '<div id="footer-explore" class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));
	register_sidebar(array(
		'name' => 'Footer - Stay',
		'description' => __( 'Display Stay links in the footer' ),
		'before_widget' => '<div id="footer-stay" class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));
	register_sidebar(array(
		'name' => 'Footer - Contact',
		'description' => __( 'Display contact details in the footer' ),
		'before_widget' => '<div id="footer-contact" class="widget">',
		'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	));
	register_sidebar(array(
		'name' => 'Footer - Navigation',
		'description' => __( 'Display copyright and navigation in the footer' ),
		'before_widget' => '<div id="footer-navigation" class="widget">',
		'after_widget' => '</div>',
	));



/* Add theme support for Featured Images */

add_theme_support( 'post-thumbnails' );

?>